export const state = () => ({
  user: null,
  bvn: null,
  token: null,
  regStep: 0,
  authDetails: {},
})

export const mutations = {
  SET_TOKEN: (state, payload) => {
    // console.log(payload)
    state.token = payload
  },

  SET_DATA: (state, payload) => {
    state.user = payload
  },

  SET_BVN: (state, payload) => {
    state.bvn = payload
  },

  UPDATE_USER_DATA: (state, payload) => {
    state.user = { ...state.user, ...payload }
  },

  CLEAR_BVN: (state, payload) => {
    state.bvn = null
  },

  LOGOUT: (state, { path }) => {
    state.user = {}
    state.token = null
    state.bvn = null
    localStorage.removeItem('core-bank-client-v1')
    window.location = path || '/'
  },

  UPDATE_REG_STEP: (state, payload) => {
    state.regStep = payload
  },

  UPDATE_AUTH_DATA: (state, payload) => {
    state.authDetails = {
      ...state.authDetails,
      ...payload,
    }
  },

  CLEAR_AUTH_DETAILS: (state, payload) => {
    state.authDetails = {}
  },
}
export const actions = {
  async logout({ state }) {},
}

export const getters = {
  isLoggedIn: (state) => !!state.token,
  user: (state) => state.user,
  token: (state) => state.token,
  bvn: (state) => state.bvn,
  authDetails: (state) => state.authDetails,
}
