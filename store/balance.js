export const state = () => ({
  balanceHidden: false,
})

export const mutations = {
  TOGGLE_BALANCE: (state) => {
    // console.log(state)
    state.balanceHidden = !state.balanceHidden
  },
  SHOW_BALANCE: (state) => {
    state.balanceHidden = false
  },
}
export const actions = {}

export const getters = {
  isBalanceHidden: (state) => state.balanceHidden,
}
