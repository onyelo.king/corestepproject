// import toast from "../helpers/toast";
/* eslint-disable */
export default function ({ $axios, store, app }) {
  let token = ''

  $axios.defaults.headers.common['Content-Type'] = 'application/json'
  $axios.defaults.headers.common.Accept = 'application/json'

  $axios.onRequest((config) => {
    // console.log('Making request to ' + config.url)
    // console.log(store.getters, store.getters.token)
    if (window.localStorage.getItem('core-bank-v2') !== null) {
      token = store.getters['auth/token']
    } else if (store.getters['auth/token'] !== null) {
      token = store.getters['auth/token']
    }

    // $axios.setHeader('Authorization', token)
    $axios.defaults.credentials = 'include'

    if (token) {
      $axios.defaults.headers.common.Authorization = `${token}`
    }
    // console.log(config)
  })

  $axios.onError((error) => {
    const code = parseInt(error.response && error.response.status)

    if (
      error.config.hasOwnProperty('errorHandle') &&
      error.config.errorHandle === false
    ) {
      return Promise.reject(error)
    }

    // console.log(error.response)
    if (code !== 200 && code !== 201 && code !== 204 && code !== 401) {
      console.log(error)
      console.log('Request Calling Error', error)
      const messages = error.response.data.errors

      for (const key in messages) {
        if (messages.hasOwnProperty(key)) {
          //   const msg = messages[key];
          toast.error(msg)
        }
      }
    }

    if (code === 401) {
      //   toast.error(error.response.data.error);
      localStorage.removeItem('core-bank-client-v1')
      window.location = '/'
    }
  })

  // Add a response interceptor
  $axios.interceptors.response.use(
    function (response) {
      // Any status code that lie within the range of 2xx cause this function to trigger
      // Do something with response data
      // console.log(response)
      if (response.headers.token) {
        console.log(response.headers.token)
        return store.commit('auth/SET_TOKEN', response.headers.token)
      }

      return response
    },
    function (error) {
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      // Do something with response error
      return Promise.reject(error)
    }
  )
}
