import Vue from 'vue'
import VeeValidate from 'vee-validate'
import VueNotification from '@mathieustan/vue-notification'
import VueContentPlaceholders from 'vue-content-placeholders'

Vue.use(VeeValidate, {
  mode: 'passive',
  classes: true,
  errorBagName: 'verrors',
  classNames: {
    valid: 'is-valid',
    invalid: 'is-invalid',
  },
})

Vue.use(VueNotification, {
  top: true,
  bottom: false,
  left: true,
  right: false,
  showClose: true,
})
Vue.use(VueContentPlaceholders)

// Filter: API Price format to naira
Vue.filter('formatPrice', function (value) {
  if (value === 0) return 0
  if (!value) return 0

  return (+value).toLocaleString('en-NG', {
    style: 'currency',
    currency: 'NGN',
  })
})

// Filter: Mask AccountNumber Details
Vue.filter('maskNumber', function (value) {
  if (value === 0) return 0
  if (!value) return 0

  return value.slice(5)
})

// Filter: format Date
Vue.filter('formatDate', function (value) {
  if (!value) return

  return new Date(value).toDateString()
})

// Filter: format Date
Vue.filter('formatTime', function (value) {
  if (!value) return

  return new Date(value).toLocaleTimeString()
})

// Filter: Formart Digits to have commas
Vue.filter('formatDigits', function (value) {
  if (value === 0) return 0
  if (!value || !Number.isInteger(value)) return

  return parseInt(value).toLocaleString()
})
